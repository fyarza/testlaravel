<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
 
    $title = $faker->sentence(4);
    return [
        'title' => $title, 
        'body' => $faker->text(500),  
        'excerpt' =>$faker->text(200), 
        'published_at' =>$faker->dateTimeBetween('-30 days', '+30 days'), 
        'category_id' =>rand(1,20), 
        'user_id' => rand(1,30),
        'url' => str_slug($title),
    ];
});
