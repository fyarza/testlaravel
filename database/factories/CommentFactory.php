<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    $title = $faker->sentence(4);
    return [
        'body' => $faker->text(500),  
        'user_id' => rand(1,20),
        'post_id' => rand(1,200),
    ];
});
