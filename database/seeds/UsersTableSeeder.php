<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class,20)->create();

        /*Usuarios Administradores*/
        $admin       = new User;
        $admin->name = 'Federico Yarza';
		$admin->email       = 'federicoyarza295@gmail.com';
		$admin->password    = Hash::make('freidnea');
        $admin->save();

        $admin->assignRoles('admin');

        $admin       = new User;
        $admin->name = 'Administrador';
        $admin->email       = 'admin@admin.com';
        $admin->password    = Hash::make('123456');
        $admin->save();

        $admin->assignRoles('admin');
    }
}
