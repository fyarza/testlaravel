<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class CreateUsuarioTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function an_authenticated_can_create_users()
    {
        $this->withoutExceptionHandling();
        // Given =>Teniendo
        $user = factory(User::class)->create(['name' => 'osorio','email'=>'g.osorio1085@gmail.com']);
        $this->actingAs($user);
        // When => Cuando hace un post request a Usuario
        $this->post(route('users.store'),['name'=>'Test',
                                            'email'=>'test1085@gmail.com',
                                            'password'=>'123456',
                                            'password_confirmation'=>'123456']);
        // Then => Entonces veo un nuevo usuario en la base de datos
        $this->assertDatabaseHas('users',[
            'name' => 'Test',
            'email' => 'test1085@gmail.com'
        ]);
    }
}
