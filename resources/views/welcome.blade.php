@extends('layouts.layout')

@section('content')
@if($posts->count())
<div class="container blog-principal">
    <!--Blog-->
    <div class="row">
        <!--Main listing-->
        <div class="col-md-12 col-12 mt-2">
            <!--Section: Blog -->
            <section class="text-justify my-5">
                @php
                    $i=1;
                @endphp
                @foreach($posts as $post)
                @if ($i == 1)
                @include('blogpartial.BlogListingV4')
                @elseif ($i == 2 or $i == 3)
                @if ($i == 2)
                <div class="row">
                    @endif
                    @include('blogpartial.BlogSecV2')
                    @if ($i==count($posts) or $i == 3 or $i == 6 or $i == 8)
                </div>
                @endif
                @elseif($i == 4)
                @include('blogpartial.BlogSecV2')
                @endif

                @php
                    $i++;
                @endphp
                @endforeach

            </section>
            <div class="d-flex justify-content-center">
                {{$posts->links()}}
            </div>
        </div>
        <!--Sidebar-->
        <div class="col-md-3 col-12 d-none d-md-block">
        </div>
    </div>
    <!--Blog-->
</div>

@else
<div class="container blog-principal" style="height:400px;margin:3rem auto;">
    <div class="row">
        <div class="col-12">
            <h1>NO HAY PUBLICACIONES</h1>
            <a class="mt-5 btn btn-info btn-block" href="{{ URL::previous() }}">Regresar</a>
        </div>
    </div>
</div>

@endif
@endsection
