@extends('layouts.app')

@section('header')
<h1>
    POSTS
    <small>Listado</small>
</h1>
@stop

@section('content')
<div class="container">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Listado de publicaciones</h3>
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">
                <i class="fa fa-plus"></i> Crear publicación
            </button>
        </div>
        @include('admin.posts.create')
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="posts-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Extracto</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->excerpt }}</td>
                        <td width="15%">
                            <a href="{{ route('posts.show', $post) }}" class="btn btn-xs btn-success" target="_blank"><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{route('posts.edit',$post)}}" class="btn btn-xs btn-info"><i
                                    class="fa fa-pencil text-white"></i></a>
                            <form method="POST" action="{{ route('posts.destroy',$post) }}" style="display: inline">
                                {{ csrf_field() }} {{ method_field('DELETE') }}
                                <button class="btn btn-xs btn-danger"
                                    onclick="return confirm('¿Estás seguro de querer eliminar esta publicación?')"><i
                                        class="fa fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
			</table>
			{{ $posts->links() }}
        </div>
        <!-- /.box-body -->
    </div>
</div>
<!-- /.box -->
@stop

@push('styles')
<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $("#posts-table").DataTable();
    });

</script>
@endpush
