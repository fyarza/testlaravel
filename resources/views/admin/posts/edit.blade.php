@extends('layouts.app')

@section('header')
	<h1>
		POSTS
		<small>Crear publicación</small>
	</h1>
@stop

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			@include('layouts.errors')
			<form id="post-form" method="POST" action="{{ route('posts.update', $post) }}">
				{{ csrf_field() }} {{ method_field('PUT') }}
				<div class="col-md-8">
					<div class="box box-primary">
						<div class="box-body">
							<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
								<label>Título de la publicación</label>
								<input name="title"
									   class="form-control"
									   value="{{ old('title', $post->title) }}"
									   {{-- value ="{{$post}}" --}}
									   placeholder="Ingresa aquí el título de la publicación">
								{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
								<label>Contenido publicación</label>
								<textarea rows="10" name="body" id="editor" class="form-control" placeholder="Ingresa el contendido completo de la publicación">{{ old('body', $post->body) }}</textarea>
								{!! $errors->first('body', '<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group {{ $errors->has('iframe') ? 'has-error' : '' }}">
								<label>Contenido embebido (iframe)</label>
								<textarea rows="2" name="iframe" id="editor" class="form-control" placeholder="Ingresa contenido embebido (iframe) de audio o video">{{ old('iframe', $post->iframe) }}</textarea>
								{!! $errors->first('iframe', '<span class="help-block">:message</span>') !!}
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box box-primary">
						<div class="box-body">
							<div class="form-group">
								<label>Fecha de publicación:</label>
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input name="published_at"
										   class="form-control pull-right"
										   value="{{ old('published_at', $post->published_at ? $post->published_at->format('m/d/Y') : null) }}"
										   type="text"
										   id="datepicker">
								</div>
							</div>
							<div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
								<label>Categorías</label>
								<select name="category_id" class="form-control select2">
									<option value="">Seleciona una categoría</option>
									@foreach ($categories as $category)
										<option value="{{ $category->id }}"
												{{ old('category_id', $post->category_id) == $category->id ? 'selected' : '' }}
										>{{ $category->name }}</option>
									@endforeach
								</select>
								{!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
								<label>Etiquetas</label>
								<select name="tags[]" class="form-control select2"
										multiple="multiple"
										data-placeholder="Selecciona una o más etiquetas" style="width: 100%;">
									@foreach ($tags as $tag)
										<option {{ collect(old('tags', $post->tags->pluck('id')))->contains($tag->id) ? 'selected' : '' }} value={{ $tag->id }}>{{ $tag->name }}</option>
									@endforeach
								</select>
								{!! $errors->first('tags', '<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group {{ $errors->has('excerpt') ? 'has-error' : '' }}">
								<label>Extracto publicación</label>
								<textarea name="excerpt"
										  class="form-control"
										  placeholder="Ingresa un extracto de la publicación">{{ old('excerpt', $post->excerpt) }}</textarea>
								{!! $errors->first('excerpt', '<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-group">
								<div class="dropzone"></div>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Guardar Publicación</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
	
@stop

@push('styles')
<link rel="stylesheet" href="{{asset('plugins/admin/css/dropzone.css')}}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/datepicker/datepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('/adminlte/plugins/select2/select2.min.css') }}">
@endpush

@push('scripts')
<script src="{{asset('plugins/admin/js/dropzone.min.js')}}"></script>
<script src="{{asset('plugins/admin/tinymce_4.2.7/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('/adminlte/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>

 <script>
	var  webdingsStyle ="@font-face {" +
					"font-family: 'Wingdings';" +
					"src: url('{{asset('plugins/admin/tinymce_4.2.7/fonts/kopanowebappdings.eot')}}');" +
					"src: url('{{asset('plugins/admin/tinymce_4.2.7/fonts/kopanowebappdings.eot?#iefix')}}') format('embedded-opentype')," +
						"url('{{asset('plugins/admin/tinymce_4.2.7/fonts/kopanowebappdings.woff2')}}') format('woff2')," +
						"url('{{asset('plugins/admin/tinymce_4.2.7/fonts/kopanowebappdings.woff')}}') format('woff')," +
						"url('{{asset('plugins/admin/tinymce_4.2.7/fonts/kopanowebappdings.ttf')}}') format('truetype');" +
					"font-weight: normal;" +
					"font-style: normal;" +
				"}";
    tinymce.init({
        selector: "#editor",
        theme: "modern",
        width: 720,
        height: 300,
        delta_height: 1,
		plugins: ["advlist emoticons directionality lists image charmap searchreplace textcolor table powerpaste link preview"],
		link_assume_external_targets: true,
		powerpaste_word_import: true,
		powerpaste_html_import: true,
		powerpaste_allow_local_images: true,
		toolbar1 : "fontselect fontsizeselect | bold italic underline strikethrough | subscript superscript | forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | ltr rtl | bullist numlist | table | searchreplace | link unlink | undo redo | charmap emoticons image hr removeformat | print preview media fullpage",
		extended_valid_elements : 'a[name|href|target|title|onclick|dir],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|style],table[style|dir|class|border=1|cellspacing|cellpadding|bgcolor|id],colgroup,col[style|dir|width],tbody,tr[style|dir|class],td[style|dir|class|colspan|rowspan|width|height],hr[class|width|size|noshade],font[face|size|color|style|dir],span[class|align|style|dir|br],p[class|style|dir|span|br]',
		paste_data_images : true,
		automatic_uploads: false,
		remove_trailing_brs: false,
		valid_children : '+body[style]',
		font_formats : 'Arial=arial,helvetica,sans-serif; Courier New=courier new,courier,monospace; AkrutiKndPadmini=Akpdmi-n',
		fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
		browser_spellcheck : true,
		menubar : false,
		statusbar : false,
		visual_anchor_class : 'zarafa_tinymce_anchor',
		relative_urls : false,
		remove_script_host : false,
		forced_root_block: 'P',
		forced_root_block_attrs: {
			'style' : 'padding: 0; margin: 0; '
		},
		content_style :
			webdingsStyle +
			'body{ '+
				'word-wrap: break-word;' +
			'}' +
			'p,blockquote{'+
				'font-family : initial;'+
				'font-size : medium;'+
			'}'+
			'td, th, p{' +
				'font-family : inherit !important;' +
				'font-size : inherit !important;' +
			'}',
		table_default_styles: {
			width: '10%',
			borderSpacing: 0
		}
	
    }); 
</script>  
<script>
    $('#datepicker').datepicker({
        autoclose: true
    });

    $('.select2').select2({
    	tags: true
    });
    var myDropzone = new Dropzone('.dropzone', {
        url: '/admin/posts/{{ $post->url }}/photos',
        paramName: 'photo',
        acceptedFiles: 'image/*',
        maxFilesize: 2,
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        dictDefaultMessage: 'Arrastra las fotos aquí para subirlas'
    });

    myDropzone.on('error', function(file, res){
        var msg = res.photo[0];
        $('.dz-error-message:last > span').text(msg);
    });

    Dropzone.autoDiscover = false;

</script>
@endpush
