@extends('admin.layout')
@push('styles')
    {{--Select 2 --}}
    <link rel="stylesheet"
          href="{{ asset('/plugins/admin/vendors/bower_components/select2/dist/css/select2.min.css') }}">
    {{--date--}}
    <link rel="stylesheet"
          href="{{ asset('/plugins/admin/vendors/bower_components/flatpickr/dist/flatpickr.min.css') }}">
    {{--Summernote--}}
    <link rel="stylesheet" href="{{ asset('plugins/admin/vendors/bower_components/summernote/summernote-bs4.css')}}">
@endpush
@section('header')
    <header class="content__title">
        <h1>{{ config('constant.BLOGUERO') }} </h1>
        {{--<small> Crear Posts</small>--}}
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.admin.posts.index') }}">Posts</a></li>
                <li class="breadcrumb-item active" aria-current="page">Crear post</li>
            </ol>
        </nav>
        {{--<h4 class="card-title">Crear publicación</h4>--}}
        <div class="actions">
            <a href="" class="actions__item zmdi zmdi-trending-up"></a>
            <a href="" class="actions__item zmdi zmdi-check-all"></a>

            <div class="dropdown actions__item">
                <i data-toggle="dropdown" class="zmdi zmdi-more-vert"></i>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="" class="dropdown-item">Refresh</a>
                    <a href="" class="dropdown-item">Manage Widgets</a>
                    <a href="" class="dropdown-item">Settings</a>
                </div>
            </div>
        </div>

    </header>
@stop

@section('content')
    {{--WEB--}}
    <form class="row" method="POST" action="{{ route('admin.admin.posts.store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{--<div class="row">--}}

        <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="card-body">
                    <div class="form-group {{ $errors->has('title') ? 'has-warning' : '' }}">
                        <h4 class="card-title">Título de la publicación</h4>
                        <input name="title"
                               value="{{old('title')}}"
                               class="form-control textarea-autosize text-counter"
                               data-max-length="45"
                               data-min-length="10"
                               placeholder="(Max cantidad de caracteres 45)"/>
                        {{ $errors->first('title'), '<span class="help-block">:message</span>' }}
                        <i class="form-group__bar"></i>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="form-group {{ $errors->has('body') ? 'has-warning' : '' }}">
                        <h4 class="card-title">Contenido completo de la publicación</h4>
                        <textarea name="body"
                                  id="summernote"
                                  rows="10"
                                  class="form-control"
                                  placeholder="Hola {{ auth()->user()->name }}, ya puedes iniciar... ">
                            {{old('body')}}
                        </textarea>
                        {{ $errors->first('body'), '<span class="help-block">:message</span>' }}
                        <i class="form-group__bar"></i>
                    </div>
                </div>
            </div>
        </div>
        {{--FIN WEB--}}
        {{--ASIDE--}}
        <div class="col-lg-4 col-md-5 hidden-md-down">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Fecha de publicación</h4>
                    <h6 class="card-subtitle">Si no colocas fecha el post quedara guardado pero no se publicara</h6>
                    <div>
                        {{--<label>Fecha de publicación</label>--}}
                        <div class="input-group mb-2 pb-2">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i></span>
                            <div class="form-group">
                                <input type="date" name="published_at" value="{{old('published_at')}}" class="form-control datetime-picker flatpickr-input"
                                       placeholder="Insertar Fecha" readonly="readonly">
                                <i class="form-group__bar"></i>
                            </div>
                           {{-- <div class="form-group {{ $errors->has('time') ? 'has-warning' : '' }}">
                                <input name="time"  value="{{old('time')}}"  type="time" class="form-control time-picker flatpickr-input"
                                       placeholder="Insertar hora" readonly="readonly">
                                {{ $errors->first('time'), '<span class="help-block">:message</span>' }}
                                <i class="form-group__bar"></i>
                            </div>--}}
                        </div>
                        <div class="form-group mb-3 pb-3 {{ $errors->has('category') ? 'has-warning' : '' }}">
                            <h4 class="card-title">Categoría</h4>

                            <select name="category" class="select2">
                                <option value="">Elegir categoría</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                    {{old('category') == $category->id ? 'selected' : '' }}>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                            {{ $errors->first('category'), '<span class="help-block">:message</span>' }}
                        </div>

                        <div class="form-group mb-3 pb-3 {{ $errors->has('tags') ? 'has-warning' : '' }}">
                            <h4 class="card-title">Etiquetas</h4>

                            <select name="tags[]" class="select2 select2-hidden-accessible" multiple=""
                                    tabindex="-1" aria-hidden="true">

                                @foreach($tags as $tag)
                                    <option {{ collect(old('tags'))->contains($tag->id) ? 'selected' : ''}} value="{{ $tag->id }}">{{ $tag->name }}</option>
                                @endforeach
                            </select><span
                                    class="select2 select2-container select2-container--default select2-container--below"
                                    dir="ltr" style="width: 100%;"><span class="selection"><span
                                            class="select2-selection select2-selection--multiple" role="combobox"
                                            aria-haspopup="true" aria-expanded="false" tabindex="-1"><ul
                                                class="select2-selection__rendered"><li
                                                    class="select2-search select2-search--inline"><input
                                                        class="select2-search__field" type="search" tabindex="0"
                                                        autocomplete="off" autocorrect="off" autocapitalize="off"
                                                        spellcheck="false" role="textbox" aria-autocomplete="list"
                                                        placeholder=""
                                                        style="width: 0.75em;"></li></ul></span></span><span
                                        class="dropdown-wrapper" aria-hidden="true"></span></span>
                            {{ $errors->first('tags'), '<span class="help-block">:message</span>' }}
                        </div>

                        <div class="form-group mt-2 {{ $errors->has('excerpt') ? 'has-warning' : '' }}">
                            <h4 class="card-title">Extracto de la publicación</h4>
                            <textarea
                                    name="excerpt"
                                    class="form-control textarea-autosize text-counter"
                                    data-max-length="275" data-min-length="140"
                                    placeholder="Puedes iniciar... (Max cantidad de caracteres 275 y minimo 140)">
                                {{ old('excerpt') }}
                            </textarea>
                            {{ $errors->first('excerpt'), '<span class="help-block">:message</span>' }}
                            <i class="form-group__bar"></i>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-light btn-block" type="submit">Guardar Publicación</button>
                        </div>
                    </div>

                    {{--<div>
                        <label>Date picker</label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                            <div class="form-group">
                                <input type="text" class="form-control date-picker flatpickr-input" placeholder="Pick a date" readonly="readonly">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>

                    <div>
                        <label>Time picker</label>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
                            <div class="form-group">
                                <input type="text" class="form-control time-picker flatpickr-input" placeholder="Pick a time" readonly="readonly">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>--}}

                </div>
            </div>
        </div>

        {{--</div>--}}
    </form>

@stop
@push('script')
    {{--create posts--}}
    {{--<script src="{{ asset('/plugins/admin/vendors/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js') }}"></script>--}}
    <script src="{{ asset('/plugins/admin/vendors/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    {{--<script src="{{ asset('/plugins/admin/vendors/bower_components/dropzone/dist/min/dropzone.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/plugins/admin/vendors/bower_components/moment/min/moment.min.js') }}"></script>--}}
    <script src="{{ asset('/plugins/admin/vendors/bower_components/flatpickr/dist/flatpickr.min.js') }}"></script>
    {{--<script src="{{ asset('/plugins/admin/vendors/bower_components/nouislider/distribute/nouislider.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/plugins/admin/vendors/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/plugins/admin/vendors/bower_components/trumbowyg/dist/trumbowyg.min.js') }}"></script>--}}
    <script src="{{asset('plugins/admin/vendors/bower_components/summernote/summernote-bs4.js')}}"></script>
    <script src="{{asset('plugins/admin/vendors/bower_components/summernote/summernote-image-title.js')}}"></script>
    <script src="{{asset('plugins/admin/vendors/bower_components/summernote/lang/summernote-es-ES.js')}}"></script>
    <script src="{{ asset('/plugins/admin/vendors/bower_components/tinymce/tinymce.min.js') }}"></script>
    {{--<script src="{{ asset('/plugins/admin/vendors/bower_components/rateYo/min/jquery.rateyo.min.js') }}"></script>--}}
    <script src="{{ asset('/plugins/admin/vendors/bower_components/jquery-text-counter/textcounter.min.js') }}"></script>
    <script src="{{ asset('/plugins/admin/vendors/bower_components/autosize/dist/autosize.min.js') }}"></script>



    James Camomile
    Hace 1 año
    For those like me who like to copy and wish they could copy code from a video:

    <script>
        /*$('#summernote').summernote({
            placeholder: 'Hola ',
            imageTitle: {
                specificAltField: true,
            },
            lang: 'es-ES',
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            tabsize: 6

        });*/
        $('#summernote').summernote({
            placeholder: 'Hola {{ Auth::user()->name }}',
            imageTitle: {
                specificAltField: true,
            },
            lang: 'es-ES',
            popover: {
                image: [
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']],
                    ['custom', ['imageTitle']],
                ],
            },
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            tabsize: 6
        });
    </script>﻿
@endpush
