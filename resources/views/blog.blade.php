@extends('layouts.layout')

@section('content')
@if($posts->count())
<div class="container blog-principal">
    <!--Blog-->
    <div class="row">
        <!--Main listing-->
        <div class="col-md-9 col-12 mt-2">
            <div>
                <nav class="breadcrumb" style="background-color:white;">
                    <a class="breadcrumb-item" href="/">INICIO</a>
                    <a class="breadcrumb-item active" href="{{route('pages.blog')}}">BLOG</a>
                    {{-- <a class="breadcrumb-item" href="#">{{$posts[0]->title}}</a>
                    <span class="breadcrumb-item active">{{$posts[0]->title}}</span> --}}
                </nav>
            </div>
            <!--Section: Blog -->
            <section class="text-justify my-5">
                @php
                    $i=1;
                @endphp
                @foreach($posts as $post)
                @if ($i == 1)
                @include('pages.blogpartial.BlogListingV4')
                @elseif ($i == 2 or $i == 3)
                @if ($i == 2)
                <div class="row text-center my-5 p-5">
                    @endif
                    @if (count($post->photos)>0)
                    @include('pages.blogpartial.BlogSecV1')
                    @else
                    @include('pages.blogpartial.BlogSecV2')
                    @endif
                    @if ($i==count($posts) or $i == 3 or $i == 6 or $i == 8)
                </div>
                @endif
                @elseif($i == 4)
                @if(count($post->photos)>0)
                @include('pages.blogpartial.BlogSecV1')
                @else
                @include('pages.blogpartial.BlogSecV2')
                @endif
                @endif

                @php
                    $i++;
                @endphp
                @endforeach

            </section>
            <div class="d-flex justify-content-center">
                {{-- {{ $posts->links('vendor.pagination.lecomp') }} --}}
                {{$posts->links()}}
            </div>
        </div>
        <!--Sidebar-->
        <div class="col-md-3 col-12 d-none d-md-block">
            @include('pages.partial.aside')
        </div>
    </div>
    <!--Blog-->
</div>

@else
<div class="container blog-principal" style="height:400px;margin:3rem auto;">
    <div class="row">
        <div class="col-12">
            <h1>NO HAY PUBLICACIONES</h1>
            <a class="mt-5 btn btn-info btn-block" href="{{ URL::previous() }}">Regresar</a>
        </div>
    </div>
</div>

@endif


<!--Main layout-->
<!--Intro-->

@stop