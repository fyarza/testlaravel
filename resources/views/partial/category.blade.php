@foreach($categories as $category)
    @if ($category->posts->count() > 0)
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <a class="">
                <p class="mb-0">
                    <a href="{{ route('categories.show', $category) }}" class="dark-grey-text">
                        {{ $category->name }}
                    </a>
                </p>
            </a>
            <span class="badge badge-teal badge-pill font-small" style="color:black !important;">{{ $category->posts->count() }}</span>
        </li>
    @endif
@endforeach