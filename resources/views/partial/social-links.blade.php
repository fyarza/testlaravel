<div class="row justify-content-center my-5">
    <!--Facebook-->
    <a href="https://www.facebook.com/sharer.php?u={{ request()->fullUrl() }}&title={{ $description }}"
       target="_blank" >
       <button class="btn btn-fb btn-sm waves-effect waves-light">
        <i class="fa fa-facebook left "></i>
        <span class="clearfix d-none d-md-inline-block">Facebook</span>
    </button>
    </a>


<!--Twitter-->
    <a href="https://twitter.com/intent/tweet?url={{ request()->fullUrl() }}&text={{ $description }}&via={{ config('app.name') }}&hashtags={{ config('app.name') }}" target="_blank"
       >
       <button class="btn btn-tw btn-sm waves-effect waves-light">
        <i class="fa fa-twitter left "></i>
        <span class="clearfix d-none d-md-inline-block">Twitter</span>
        </button>
    </a>


<!--Google+-->
    <a href="https://plus.google.com/share?url={{ request()->fullUrl() }}" 
    target="_blank" title="Share on Google+" >
    <button class="btn btn-gplus btn-sm waves-effect waves-light">
    <i class="fa fa-google-plus left "></i>
    <span class="clearfix d-none d-md-inline-block">Google+</span>
    </button>
    </a>

    <!--Pinterest+-->
    {{-- <a href="http://pinterest.com/pin/create/button/?url={{ request()->fullUrl() }}&description=$description" 
    target="_blank" title="Share on Pinterest" class="btn info">
        <i class="fa fa-google-plus left "></i>
        <span class="clearfix d-none d-md-inline-block">Pinterest</span>
    </a> --}}

</div>