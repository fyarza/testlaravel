
<!--Popular posts-->
@if($categories->count())
<!-- Section: Categories -->
<section class="section my-5">
    <div class="single-post">
        <p class="font-bold  text-center  py-2 mb-4">
            <strong>CATEGORIAS</strong>
        </p>
        <ul class="list-group my-4">
            @include('partial.category')
        </ul>
    </div>
</section>
<!-- Section: Categories -->
@endif

@if($tags->count())
<!-- Section: Categories -->
<section class="section mb-5">
    <div class="single-post">
        <p class="font-bold text-center  py-2 mb-4">
            <strong>TAGS</strong>
        </p>
        <ul class="list-group my-4">
            @include('partial.tags')
        </ul>
    </div>
</section>
<!-- Section: Categories -->
@endif