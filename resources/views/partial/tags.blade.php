@foreach($tags as $tag)
    @if ($tag->posts->count() > 0)
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <a class="">
                <p class="mb-0">
                    <a href="{{ route('tags.show', $tag) }}" class="dark-grey-text">
                        {{ $tag->name }}
                    </a>
                </p>
            </a>
            <span class="badge badge-teal badge-pill font-small" style="color:black !important;">{{ $tag->posts->count() }}</span>
        </li>
    @endif
@endforeach