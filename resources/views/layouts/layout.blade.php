<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<!-- Desarrollado por Federico Yarza
para cualquier contacto escribir por federicoyarza295@gmail.com -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('constant.BLOGUERO') }}</title>
    <meta name="description" content="{{ config('constant.DESCRIPCION') }}">
    <meta name="robots" content="all,follow">
    <meta name="author" content="{{ config('constant.BLOGUERO') }}">
    <meta name="keywords" content="{{ config('constant.BLOGUERO') }} 
    {{ config('constant.TEMA') }} {{ config('constant.KEYWORDS') }}">
    @if (Request::segment(1) != (''))
    <link rel="canonical" href="{{ config('constant.CANONICAL') }}">
    @endif
    <link rel="publisher" href="{{ config('constant.GPLUS') }}">
    <meta http-equiv="Allow" content="methods">
    <!--OpenGraph metadata-->
    <meta property="og:locale" content="es_LA">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ config('constant.BLOGUERO') }}">
    <meta property="og:site_name" content="{{ config('constant.BLOGUERO') }}  {{ config('constant.TEMA') }}">
    <meta property="og:url" content="{{ config('constant.MAINSITE') }}">
    <meta property="og:image" content=" {{ config('constant.LOGOS') }}">
    <meta property="article:published_time" content="{{ config('constant.PUBLICADO') }}">
    <meta property="article:author" content="{{ config('constant.MAINSITE') }}">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="{{ config('constant.TWITTER') }}">
    <meta name="twitter:title" content="{{ config('constant.BLOGUERO') }} | {{ config('constant.TEMA') }}">
    <meta name="twitter:description" content="{{ config('constant.DESCRIPCION') }}">
    <meta property="fb:admins" content="{1468470589867989}" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon/favicon-16x16.png') }}">
    <link rel="shortcut icon" href="{{asset('/favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('/favicon/favicon.ico')}}" type="image/x-icon">
    <link rel="manifest" href="{{ asset('/favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Estilos Propios -->
    <link href="{{asset('css/pagina.css')}}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    @stack('styles')
</head>

<body>
    <!-- Seccion de Navegacion -->
    <section id="encabezado">
        <!--INICIO DEL Navbar-->
        @include('partial.nav')
        <!--/.FIN DEL Navbar-->
    </section>
    <!-- Fin de Seccion de navegacion -->
    {{--contenido--}}
    @yield('content')

    <!-- Inicio del Footer -->
    <footer>
    </footer>
    <!-- Fin de Footer -->
    <!-- Inicio de la Fanja de Footer -->
    <div class="franja container-fluid">
        <div class="row">
            <a href="#">
                <p class="text-center">© 2020 Desarrollado por Federico
                    Yarza</p>
            </a>
        </div>
    </div>
    <!-- Fin  de la Fanja de Footer -->

    <!-- Ir Arriba -->
    <a data-scroll class="ir-arriba" href="#encabezado">
        <i class="fa  fa-arrow-circle-up" aria-hidden="true"> </i>
    </a>
    <!-- Fin de Ir Arriba -->
    <script src="{{asset('js/pagina.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".owl-carousel").owlCarousel({
                loop: true,
                margin: 10,
                autoplay: true,
                nav: false,
                autoplayTimeout: 4000,
                autoplayHoverPause: true,
                /*navText: ['<i class="fa fa-arrow-circle-left" title="Anterior"></i>',
                    '<i class="fa  fa-arrow-circle-right" title="Siguiente"></i>'
                ],*/
                responsive: {
                    0: {
                        items: 1,
                        margin: 20
                    },
                    500: {
                        items: 2,
                        margin: 20
                    },
                    800: {
                        items: 5,
                        margin: 20
                    },
                    1000: {
                        items: 5,
                        margin: 20
                    }
                }
            });
        });
    </script>
    @stack('script')
</body>

</html>