@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <section class="content-header">
                <h1>
                Role view
                </h1>
            </section>
            <div class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="container">
                            <div class="row justify-content-center">
                                @include('roles.show_fields')
                                <div class="col-12">
                                    <a href="{!! route('roles.index') !!}" class="btn btn-danger">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   
@endsection
