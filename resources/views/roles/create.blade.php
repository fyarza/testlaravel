@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <section class="content-header">
                <h1>
                    Create Roles
                </h1>
            </section>
            <div class="content">
                @include('layouts.errors')
                <div class="box box-primary">
            
                    <div class="box-body">
                        <div class="container">
                            <div class="row justify-content-center" >
                                <div class="col-sm-6">
                                    {!! Form::open(['route' => 'roles.store']) !!}
                
                                    @include('roles.fields')
                
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
