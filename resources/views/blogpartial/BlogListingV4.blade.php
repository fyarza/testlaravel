<article class="blog-general">
    <!--Featured image-->
    {{-- @foreach($post->photos as $photo)
    <a rel="bookmark" href="{{ route('posts.show', $post) }}">
    <img src="{{ asset('/storage/' . $photo->url) }}" 
    alt="{{ config('constant.BLOGUERO') }} - {{$post->title}}" class="view overlay rounded z-depth-2 img-fluid">
    </a>
    @endforeach --}}
    @include( $post->viewType() )
    <!--Post data-->
    <div class="text-center my-5">
        <h2>
            <a rel="bookmark" href="{{ route('posts.show', $post) }}" class="font-weight-bold">{{$post->title}}</a>
        </h2>
        <p>Escrito por:
            <a rel="bookmark" href="{{ route::has('pages.about') }}">{{ config('constant.BLOGUERO') }}</a>, {{
            $post->created_at->diffForHumans() }}
        </p>
        <!--Social shares-->
        @include('partial.social-links', ['description' => $post->title])
        <!--Social shares-->
    </div>
        {{$post->excerpt}}
        <p class="text-right mb-0 text-uppercase font-small spacing font-weight-bold">
            <a rel="bookmark" href="{{ route('posts.show', $post) }}">Leer más
                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
            </a>
        </p>
</article>