<!--Grid column-->
<div class="col-md-6 mb-4">
    <!--Card-->
    <div class="card text-left">

        <!--Card image-->
        <div class="view overlay">
            {{-- @foreach($post->photos as $photo)
                <img class="img-responsive blog__mxh"
                     src="{{ asset('/storage/' . $photo->url) }}"
                     alt="{{ config('constant.BLOGUERO') }} - {{$post->title}}">
            @endforeach

            <a>
                <div class="mask rgba-white-slight"></div>
            </a> --}}
            @include( $post->viewType() )
        </div>
        <!--/.Card image-->

        <!--Card content-->
        <div class="card-body mx-4">
            <a href="" class="teal-text text-center text-uppercase font-small">
                <h6 class="mb-3 mt-3"> hashtag: <p>
                        @foreach( $post->tags as $tag )
                            @if ($tag->count() > 0)
                                <a href="{{ route('tags.show', $tag) }}">
                                    <strong> #{{ $tag->name }} </strong>
                                </a>
                            @endif
                        @endforeach
                        <br>
                        <hr>
                        <p>Escrito por:
                            <a rel="bookmark"
                               href="{{ route::has('pages.about') }}">{{ config('constant.BLOGUERO') }}</a>, <br>
                    <a class="dark-grey-text font-small"> el {{ $post->created_at->diffForHumans() }}</a>
                        </p>
                </h6>
            </a>
            <!--Title-->
            <h4 class="card-title">
                <strong>{{$post->title}}</strong>
            </h4>
            <hr>
            <!--Text-->
            <p class="dark-grey-text mb-4">{{$post->excerpt}}.</p>

            <p class="text-right mb-0 text-uppercase font-small spacing font-weight-bold">
                <a rel="bookmark"
                   href="{{ route('posts.show', $post) }}">Leer más
                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                </a>
            </p>
            @include('partials.social-links', ['description' => $post->title])
        </div>
        <!--/.Card content-->

    </div>
    <!--/.Card-->

</div>
<!--Grid column-->