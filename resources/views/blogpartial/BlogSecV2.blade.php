<div class="row bg-white">
        <!--Grid column-->
        <div class="col-lg-12 col-xl-12">
            <!--Excerpt-->
            <p class="tags"><i class="fa fa-tags"></i>
                @foreach( $post->tags as $tag )
                    @if ($tag->count() > 0)
                            {{-- <a href="{{ route('tags.show', $tag) }}">
                                <strong> #{{ $tag->name }} </strong>
                            </a> --}}
                            <a href="#" rel="tag">{{ $tag->name }}</a> 
                    @endif
                @endforeach
            </p>

            <h3 class="mb-4 font-weight-bold lecomp-color">
                <strong>{{$post->title}}</strong>
            </h3>
            <p>
                {!!$post->excerpt!!}.
            </p>
            <p> by
            <a rel="bookmark" href="{{ route::has('pages.about') }}"> {{ config('constant.BLOGUERO') }}
            </a>, <br>
                <a class="dark-grey-text font-small"> el {{ $post->created_at->diffForHumans() }}</a>
            </p>
            <p class="text-right mb-0 text-uppercase font-small spacing font-weight-bold">
                <a rel="bookmark"
                   href="{{ route('posts.show', $post) }}">Leer más
                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                </a>
                @include('partial.social-links', ['description' => $post->title])
            </p>
        </div>
        <!--Grid column-->
</div>
