@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-8">
            <section class="content-header">
                <h1 class="pull-left">Users Manager</h1>
                <h1 class="pull-right">
                    @can('users.create')
                    <a class="btn btn-success" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Add New</a>
                    @endcan
                </h1>
            </section>
            <div class="content">
                <div class="clearfix"></div>
        
                @include('flash::message')
        
                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">
                            @include('usuarios.table')
                    </div>
                </div>
                <div class="text-center">
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

