{{-- <div class="gallery-photos masonry">
    @foreach($post->photos->take(4) as $photo)

        <figure class="gallery-image">
            @if($loop->iteration === 4 )
                <div class="overlay">{{ $post->photos->count() }} Fotos</div>
            @endif
            <img class="overlay-img" src="/publicacion/{{ $photo->url }}" alt=""></figure>

    @endforeach
</div> --}}

{{-- <div class="grid gallery-photos" >
    @foreach($post->photos->take(4) as $key=>$photo)
        <figure class="grid-item grid-item--width2">
            @if($key === 4)
                <div class="overlay">{{ $post->photos->count() }} Fotos</div>
            @endif
            <img src="/publicacion/{{ $photo->url }}" class="img-responsive" alt="">
        </figure>
    @endforeach
</div> --}}

<div class="grid">
 @foreach($post->photos->take(4) as $key=>$photo)
        <div class="grid-item grid-item--width2">
            @if($key === 4)
                <div class="overlay">{{ $post->photos->count() }} Fotos</div>
            @endif
            <img src="/publicacion/{{ $photo->url }}" class="img-responsive" alt="">
        </div>
    @endforeach
</div>

