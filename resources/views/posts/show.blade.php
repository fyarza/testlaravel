@extends('layouts.app')
@section('meta-title', $post->title)
@section('meta-description', $post->excerpt)
@push('styles')
{{--
<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}"> --}}
<style>
    .grid-item {
        float: left;
        width: 20%;
        height: 100px;
        background: #C09;
        border: 2px solid hsla(0, 0%, 0%, 0.5);
    }

    .grid-item--width2 {
        width: 40%;
    }

    .grid-item--height2 {
        height: 200px;
    }

</style>
@section('content')
<!--Blog section-->

<section>
    <div class="container-fluid grey lighten-4">
        <hr class="my-1">
        <div class="container-fluid">

            <!--Blog-->
            <div class="row mt-5 pt-1">

                <!--Main listing-->
                <div class="col-lg-10 col-12 mt-1">

                    <!--Section: Blog v.3-->
                    <section class=" pb-2  text-lg-left">

                        <!--Grid row-->
                        <div class="row mb-1">

                            <!--Grid column-->
                            <div class="col-md-12">
                                <!--Card-->
                                <div class="card postshow">
                                    <div class="grid" data-packery='{ "itemSelector": ".grid-item", "gutter": 10 }'>
                                        {{-- Incluye la imagen o carousel de imagen --}}
                                        {{-- @include( $post->viewType() ) --}}
                                    <!--Card content-->
                                    <div class="card-body2 mt-5">
                                        <!--Title-->
                                        <div class="row">
                                            <div class="col-12">
                                                <h4 class="card-title">
                                                    <strong>{{$post->title }}</strong>
                                                </h4>
                                                <hr>
                                                <!--Text-->
                                                {!!$post->body !!}
                                                {{-- <p class="dark-grey-text mb-3 mt-4 mx-4">{!! $post->excerpt !!}.</p>
                                                --}}
                                                <hr>
                                            </div>
                                        </div>
                                        <!--Grid row-->
                                        <div class="row mb-4">

                                            <!--Grid column-->
                                            <div class="col-md-12 text-center">

                                                <h4 class="text-center font-bold dark-grey-text mt-3 mb-3">
                                                    <strong>Comparte esté post: </strong>
                                                </h4>
                                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                                <div class="addthis_inline_share_toolbox"></div>

                                                <a href="https://facebook.com/"
                                                    target="_blank" rel="nofollow">
                                                    <button type="button" class="btn btn-fb btn-sm">
                                                        <i class="fa fa-facebook left"></i> Facebook
                                                    </button>
                                                </a>

                                                <!--Twitter-->
                                                <a href="https://twitter.com/"
                                                    target="_blank" rel="nofollow">
                                                    <button type="button" class="btn btn-tw btn-sm">
                                                        <i class="fa fa-twitter left"></i> Twitter
                                                    </button>
                                                </a>
                                                <!--Google +-->
                                                <a href="https://plus.google.com/"
                                                    target="_blank" rel="nofollow">
                                                    <button type="button" class="btn btn-gplus btn-sm">
                                                        <i class="fa fa-google-plus left"></i> Google +
                                                    </button>
                                                </a>

                                            </div>
                                            <!--Grid column-->

                                        </div>

                                        <a class="btn btn-info btn-block" href="{{ URL::previous() }}">Regresar</a>

                                        <!--/.Card content-->

                                    </div>
                                    <!--/.Card-->

                                </div>
                                <!--Grid column-->

                            </div>
                            <!--/Grid row-->

                    </section>
                    <!--Section: Blog v.3-->
                </div>
                <!--Main listing-->

                <!--Sidebar-->

                <!--Sidebar-->

            </div>
            <!--Blog-->

        </div>

</section>


@stop
