<div class="container">
    <div class="row justify-content-center ">
        <div class="col-12  col-lg-10">
            <figure class="view overlay rounded z-depth-2">
                <img src="/publicacion/{{ $post->photos->first()->url }}" alt="{{ config('constant.BLOGUERO') }} - {{$post->title}}"
                    class="img-fluid card-img-top">
                <a href="{{ route('posts.show', $post) }}">
                    <div class="mask rgba-white-slight waves-effect waves-light"></div>
                </a>
            </figure>
        </div>
    </div>
</div>
