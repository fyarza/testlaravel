@extends('layouts.layout')
@section('meta-title', $post->title)
@section('meta-description', $post->excerpt)
@push('styles')

@section('content')
<!--Main Navigation-->
<div class="container-fluid blog-especifico" >
    <!--Blog-->
    <div class="row mt-5">
        <!--Main listing-->
        <div class="col-md-9 col-12">
            <!--Section: Blog v.3-->
            <section class="extra-margins pb-5  text-lg-left">
                <!--Grid row-->
                <div class="row mb-4">
                    <!--Grid column-->
                    <div class="col-md-12">
                        <!--Card-->
                        <div class="card">
                            <!--Card image-->
                            <div class="view overlay">
                                @foreach($post->photos as $photo)
                                <img src="{{ asset('/publicacion/' . $photo->url) }}" class="img-fluid" alt="">
                                <a>
                                    <div class="mask rgba-white-slight"></div>
                                </a>
                                @endforeach
                            </div>
                            <!--/.Card image-->

                            <!--Card content-->
                            <div class="card-body text-justify" id="prueba">
                                <!--Title-->
                                <h4 class="card-title">
                                    <strong>{{$post->title . ' | ' .
                                        config('constant.BLOGUERO') }}</strong>
                                </h4>
                                <hr>
                                <!--Text-->
                                {!!
                                    $post->body !!}

                                <hr>

                                <!--Grid row-->
                                
                                <div class="row mb-4">

                                    <!--Grid column-->
                                    <div class="col-md-12 text-center" id="redesblogesp">

                                        <h4 class="text-center font-weight-bold dark-grey-text mt-3 mb-3">
                                            <strong>Compartir este post: </strong>
                                        </h4>

                                        <a href="https://www.facebook.com/sharer.php?u={{ request()->fullUrl() }}&title={{ $post->title }}"
                                            target="_blank" type="button" class="btn info">
                                            <i class="fa fa-facebook left"></i>
                                            Facebook</a>

                                        <!--Twitter-->
                                        <a href="https://twitter.com/intent/tweet?url={{ request()->fullUrl() }}&text={{ $post->title }}&via={{ config('app.name') }}&hashtags={{ config('app.name') }}"
                                            target="_blank" type="button" class="btn info">
                                            <i class="fa fa-twitter left"></i>
                                            Twitter</a>

                                        <!--Google +-->
                                        <a href="https://plus.google.com/share?url={{ request()->fullUrl() }}" target="_blank"
                                            title="Share on Google+" type="button" class="btn info">
                                            <i class="fa fa-google-plus left"></i>
                                            Google +</a>

                                        <!--Google +-->
                                        <a href="http://pinterest.com/pin/create/button/?url={{ request()->fullUrl() }}&description=$description"
                                            target="_blank" title="Share on Pinterest" type="button" class="btn info">
                                            <i class="fa fa-pinterest left"></i>
                                            Pinterest</a>

                                    </div>
                                    <!--Grid column-->

                                </div>
                                <!--Grid row-->

                            </div>
                            <!--/.Card content-->

                        </div>
                        <!--/.Card-->

                    </div>
                    <!--Grid column-->

                </div>
                <!--/Grid row-->
        </div>
        <!--Main listing-->
        <!--Sidebar-->
        <div class="col-md-3 col-12 d-none d-md-block">
            @include('pages.partial.aside')
        </div>
        <!--Sidebar-->

        {{--fin aqui--}}
    </div>
    <!--Grid column-->
    @stop

    @push('scripts')
    <script type="text/javascript">
        // Material Select Initialization
        $(document).ready(function () {
            $('.mdb-select').material_select();
        });
    </script>
    @endpush