/*------------------------
INICIAMOS WOW
-------------------------*/
new WOW().init();

/*----------------------------------
Iniciamos smoothScroll (Scroll Suave)
--------------------------------*/
smoothScroll.init({
    speed: 1000, // Integer. How fast to complete the scroll in milliseconds
    offset: 100, // Integer. How far to offset the scrolling anchor location in pixels

});

/*---------------------------------
    OCULTAR Y MOSTRAR BOTON IR ARRIBA
 ----------------------------------*/
$(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        $(".ir-arriba").fadeIn();
        if ($(window).width() > 768) {
            $("#encabezado .navbar").addClass('fixed-top').fadeIn(5000);
            $("#encabezado .navbar").addClass('nav-scroll').fadeIn(5000);
        }else{
            $("#encabezado .navbar").removeClass('fixed-top');
            $("#encabezado .navbar").removeClass('nav-scroll');
        } 

    } else {
        $(".ir-arriba").fadeOut();
        if ($(window).width() > 768) {
            $("#encabezado .navbar").removeClass('fixed-top');
            $("#encabezado .navbar").removeClass('nav-scroll');
        } 
       
    }
    $(window).scroll(function () {
        var scrolltop = $(this).scrollTop();
        if (scrolltop >= 50) {
            $(".ir-arriba").fadeIn();
            if ($(window).width() > 768) {
                $("#encabezado .navbar").addClass('fixed-top').fadeIn(5000);
                $("#encabezado .navbar").addClass('nav-scroll').fadeIn(5000);
            }else{
                $("#encabezado .navbar").removeClass('fixed-top');
                $("#encabezado .navbar").removeClass('nav-scroll');
            } 
        } else {
            $(".ir-arriba").fadeOut();
            if ($(window).width() > 768) {
                $("#encabezado .navbar").removeClass('fixed-top');
                $("#encabezado .navbar").removeClass('nav-scroll');
            } 
        }
    });

});


/*---------------------------------
    OCULTAR Y MOSTRAR BOTON FLECHA ABAJO
 ----------------------------------*/
$(function () {
    var scroll = $(window).scrollTop();
    if (scroll < 40) {
        $(".flecha-bajar").fadeIn();
    } else {
        $(".flecha-bajar").fadeOut();
    }
    $(window).scroll(function () {
        var scrolltop = $(this).scrollTop();
        if (scrolltop < 40) {
            $(".flecha-bajar").fadeIn();
        } else {
            $(".flecha-bajar").fadeOut();
        }
    });

});



/*---------------------------------
   CABECERA ANIMADA
 ----------------------------------*/
$(window).scroll(function () {
    var nav = $('.encabezado .container');
    var scroll = $(window).scrollTop();
    if (scroll >= 80) {
        nav.addClass("fondo-menu");
    } else {
        nav.removeClass("fondo-menu");
    }
});

/*-----------------------------------
Funcion para Agregar fondo al menu Movil
------------------------------------*/

$(window).resize(function () {
    if ($(window).width() <= 767) {
        $("#fondo-menu").addClass("fondo-movil-menu");
    } else {
        $("#fondo-menu").removeClass("fondo-movil-menu");
    }
});

$(window).on('load', function () {
    if ($(window).width() <= 767) {
        $("#fondo-menu").addClass("fondo-movil-menu");
    } else {
        $("#fondo-menu").removeClass("fondo-movil-menu");
    }
});

/*---------------------------------
Funcion para determinar si estamos 
en blog
----------------------------------- */
$(window).on('load', function () {
    if ($(".menu-blog ul li").hasClass('active')) {
        $(".bienvenidos").css("height", "15vh");
        $(".card").css("display", "inline-flex");
        $(".card").css("border", "none");
    }
    if ($(".menu-eventos ul li").hasClass('active')) {
        $(".bienvenidos").css("height", "15vh");
        $(".card").css("display", "inline-flex");
        $(".card").css("border", "none");
    }
});


/*-------------------------------------
Funcion de para cambiar de color el navbar
Funciona un Color para los Distintos navbar
-------------------------------------- */
$(function () {
    var nav = $('.encabezado');
    var blog = $("#encabezado").hasClass("blog");
    var evento = $("#encabezado").hasClass("evento");
    var blogdetallado = $("#encabezado").hasClass("blog-detallado");
    if (!blog && !evento && !blogdetallado) {
        var scroll = $(window).scrollTop();
        if (scroll > 0) {
            nav.css("background", "#fff");
        } else {
            nav.css("background", "#fff");
        }
    } else {
        nav.css("background", "#fff");
    }

    $(window).scroll(function () {
        if (!blog && !evento && !blogdetallado) {
            var scroll = $(window).scrollTop();
            if (scroll > 0) {
                nav.css("background", "#fff");
            } else {
                nav.css("background", "#fff");
            }
        } else {
            nav.css("background", "#fff");
        }

    });
});


$(window).scroll(function () {
    var nav = $('#menu-principal ul li a');
    var scroll = $(window).scrollTop();
    if (scroll > 0) {
        nav.css("font-weight", "bold");
    } else {
        nav.css("font-weight", "bold");
    }
});

// $(document).ready(function () {
//     $(window).on('load scroll', function () {
//         var scrolled = $(this).scrollTop();
//         $('#title').css({
//             'transform': 'translate3d(0, ' + -(scrolled * 0.2) + 'px, 0)', // parallax (20% scroll rate)
//             'opacity': 1 - scrolled / 400 // fade out at 400px from top
//         });
//         $('#hero-vid').css('transform', 'translate3d(0, ' + -(scrolled * 0.25) + 'px, 0)'); // parallax (25% scroll rate)
//     });

//     // video controls
//     $('#state').on('click', function () {
//         var video = $('#hero-vid').get(0);
//         var icons = $('#state > span');
//         $('#overlay').toggleClass('fade');
//         if (video.paused) {
//             video.play();
//             icons.removeClass('fa-play').addClass('fa-pause');
//         } else {
//             video.pause();
//             icons.removeClass('fa-pause').addClass('fa-play');
//         }
//     });
// });

/*----------------------------------------------
Funcion encargada de Remover las clase de Parallax
para los distintos anchos de pantallas
----------------------------------------------- */

function eliminar(numero, clase) {

    switch (numero) {
        case 320:
            clase.removeClass("parallax-480");
            clase.removeClass("parallax-800");
            clase.removeClass("parallax-1024");
            break;
        case 480:
            clase.removeClass("parallax-320");
            clase.removeClass("parallax-800");
            clase.removeClass("parallax-1024");
            break;
        case 768:
            clase.removeClass("parallax-480");
            clase.removeClass("parallax-320");
            clase.removeClass("parallax-1024");
            break;
        case 1024:
            clase.removeClass("parallax-480");
            clase.removeClass("parallax-800");
            clase.removeClass("parallax-320");
            break;
        default:
            clase.removeClass("parallax-320");
            clase.removeClass("parallax-480");
            clase.removeClass("parallax-800");
            clase.removeClass("parallax-1024");
    }
}

/*------------------------------------------
Metodo para cargar el Parallax para Distintos 
tamaños cuando se carga la pagina y modificar 
el tamaño del circulo segun el tamaño de la 
pantalla
-------------------------------------------- */
$(window).on('load', function () {
    var blog = $("#encabezado").hasClass("blog");
    var evento = $("#encabezado").hasClass("evento");
    var blogdetallado = $("#encabezado").hasClass("blog-detallado");
    if (!blog && !evento && !blogdetallado) {
        var ancho = $(window).width();
        var clase = $(".bienvenidos");
        if (ancho <= 320) {
            eliminar(320, clase);
            clase.addClass("parallax-320");
            $("#forma4").css({
                "top": "6rem",
                "left": "0rem",
                "width": "100%",
                "height": "auto",
                "padding-top": "0.5rem",
                "border-radius": "8%"
            });
        } else if ((ancho <= 480) && (ancho > 320)) {
            eliminar(480, clase);
            clase.addClass("parallax-480");
            $("#forma4").css({
                "top": "6rem",
                "left": "0rem",
                "width": "100%",
                "height": "auto",
                "padding-top": "0.5rem",
                "border-radius": "8%"
            });
            // $("#forma4").css("display", "none");
        } else if ((ancho <= 768) && (ancho > 480)) {
            eliminar(768, clase);
            clase.addClass("parallax-800");
            $("#forma4").css({
                "display": "block",
                "top": "7rem",
                "left": "3rem",
                "width": "300px",
                "height": "300px",
                "padding-top": "4rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2rem"
            });
            $("#forma4 spam").css({
                "font-size": "3.5rem"
            });
        } else if ((ancho <= 1024) && (ancho > 768)) {
            eliminar(1024, clase);
            clase.addClass("parallax-1024");
            $("#forma4").css({
                "top": "11rem",
                "left": "3rem",
                "width": "350px",
                "height": "350px",
                "padding-top": "8rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2rem"
            });
            $("#forma4 spam").css({
                "font-size": "3rem"
            });
        } else if ((ancho <= 1200) && (ancho > 1024)) {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "11rem",
                "left": "3rem",
                "width": "350px",
                "height": "350px",
                "padding-top": "8rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2rem"
            });
            $("#forma4 spam").css({
                "font-size": "4rem"
            });
        } else if ((ancho <= 1503) && (ancho > 1200)) {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "10rem",
                "left": "12rem",
                "width": "450px",
                "height": "450px",
                "padding-top": "10rem",
                "border-radius": "50%"
                
            });
            $("#forma4 h1").css({
                "font-size": "2.2rem"
            });
            $("#forma4 spam").css({
                "font-size": "5rem"
            });
        } else if ((ancho <= 2000) && (ancho > 1503)) {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "16rem",
                "left": "18rem",
                "width": "500px",
                "height": "500px",
                "padding-top": "11rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2.2rem"
            });
            $("#forma4 spam").css({
                "font-size": "5rem"
            });
        } else {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "15rem",
                "left": "22rem",
                "width": "700px",
                "height": "700px",
                "padding-top": "17rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "4rem"
            });
            $("#forma4 spam").css({
                "font-size": "8rem"
            });
        }
    }
});

/*------------------------------------------------
Metodo para cargar el Parallax para Distintos
tamaños cuando haga un RESIZE
-------------------------------------------------- */
$(window).resize(function () {
    var blog = $("#encabezado").hasClass("blog");
    var evento = $("#encabezado").hasClass("evento");
    var blogdetallado = $("#encabezado").hasClass("blog-detallado");
    if (!blog && !evento && !blogdetallado) {
        var ancho = $(window).width();
        var clase = $(".bienvenidos");
        if (ancho <= 320) {
            eliminar(320, clase);
            clase.addClass("parallax-320");
            // $("#forma4").css("display", "none");
            $("#forma4").css({
                "top": "6rem",
                "left": "0rem",
                "width": "100%",
                "height": "auto",
                "padding-top": "0.5rem",
                "border-radius": "8%"
            });
        } else if ((ancho <= 480) && (ancho > 320)) {
            eliminar(480, clase);
            clase.addClass("parallax-480");
            // $("#forma4").css("display", "none");
            $("#forma4").css({
                "top": "6rem",
                "left": "0rem",
                "width": "100%",
                "height": "auto",
                "padding-top": "0.5rem",
                "border-radius": "8%"
            });
        } else if ((ancho <= 768) && (ancho > 480)) {
            eliminar(768, clase);
            clase.addClass("parallax-800");
            $("#forma4").css({
                "display": "block",
                "top": "7rem",
                "left": "3rem",
                "width": "300px",
                "height": "300px",
                "padding-top": "4rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2rem"
            });
            $("#forma4 spam").css({
                "font-size": "3.5rem"
            });
        } else if ((ancho <= 1024) && (ancho > 768)) {
            eliminar(1024, clase);
            clase.addClass("parallax-1024");
            $("#forma4").css({
                "top": "11rem",
                "left": "3rem",
                "width": "350px",
                "height": "350px",
                "padding-top": "8rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2rem"
            });
            $("#forma4 spam").css({
                "font-size": "3rem"
            });
        } else if ((ancho <= 1200) && (ancho > 1024)) {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "11rem",
                "left": "3rem",
                "width": "350px",
                "height": "350px",
                "padding-top": "8rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2rem"
            });
            $("#forma4 spam").css({
                "font-size": "4rem"
            });
        } else if ((ancho <= 1503) && (ancho > 1200)) {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "10rem",
                "left": "12rem",
                "width": "450px",
                "height": "450px",
                "padding-top": "10rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2.2rem"
            });
            $("#forma4 spam").css({
                "font-size": "5rem"
            });
        } else if ((ancho <= 2000) && (ancho > 1503)) {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "16rem",
                "left": "18rem",
                "width": "500px",
                "height": "500px",
                "padding-top": "11rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "2.2rem"
            });
            $("#forma4 spam").css({
                "font-size": "5rem"
            });
        } else {
            eliminar(1200, clase);
            $("#forma4").css({
                "top": "15rem",
                "left": "22rem",
                "width": "700px",
                "height": "700px",
                "padding-top": "17rem",
                "border-radius": "50%"
            });
            $("#forma4 h1").css({
                "font-size": "4rem"
            });
            $("#forma4 spam").css({
                "font-size": "8rem"
            });
        }
    }
});

/*----------------------------------------------------
Funciones para el Mega Navbars 
------------------------------------------------------- */

function asignarimagen(id){
    var img= $('#imgnav');
    switch (id){
        case 1 :
        img.fadeIn(function(){
            img.attr('src','../../img/pagina/postgrado/DocAmbiental.jpg');
        });
        break;
        case 3 :
        img.fadeIn(function(){
            img.attr('src','../../img/pagina/postgrado/DocBioingenieria.jpg');
        });
        break;
        case 8:
        img.fadeIn(function(){
            img.attr('src','../../img/pagina/postgrado/DocComputoAplica.jpg');
        }); 
        break;
        case 9 :
        img.fadeIn(function(){
            img.attr('src','../../img/pagina/postgrado/DocElectrica.jpg');
        }); 
        break;
        default:
        img.fadeIn(function(){
            img.attr('src','../../img/pagina/postgrado/DocQuimica.jpg');
        }); 
  }
}


$( ".navbar .dropdown-menu.mega-menu.v-2 .sub-menu a.menu-item.img1" ).hover(
    function() {
        asignarimagen(1);
    },function(){
        
    }
)
$( ".navbar .dropdown-menu.mega-menu.v-2 .sub-menu a.menu-item.img2" ).hover(
    function() {
        asignarimagen(3);
    },function(){
        
    }
);

$( ".navbar .dropdown-menu.mega-menu.v-2 .sub-menu a.menu-item.img3" ).hover(
    function() {
        asignarimagen(8);
    },function(){
        
    }
);

$( ".navbar .dropdown-menu.mega-menu.v-2 .sub-menu a.menu-item.imgp1" ).hover(
    function() {
        asignarimagen(9);
    },function(){
     
    }
);

