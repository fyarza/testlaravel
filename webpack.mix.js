let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | pagina de administracion
 |--------------------------------------------------------------------------
 |
 */
mix.styles([
    'resources/assets/plantilla/css/font-awesome.css',
    'resources/assets/plantilla/css/simple-line-icons.min.css',
    'resources/assets/plantilla/css/style.css'
], 'public/css/plantilla.css')
.scripts([
    'resources/assets/plantilla/js/jquery.min.js',
    'resources/assets/plantilla/js/popper.min.js',
    'resources/assets/plantilla/js/pace.min.js',
    'resources/assets/plantilla/js/Chart.min.js',
    'resources/assets/plantilla/js/template.js',
    'resources/assets/plantilla/js/sweetalert2.all.min.js'
], 'public/js/plantilla.js')
.js('resources/assets/js/app.js','public/js/app.js');
/*
 |--------------------------------------------------------------------------
 | Pagina Principal
 |--------------------------------------------------------------------------
 |
 */

mix.styles([
    'resources/assets/pagina/vendor/css/tabbed/tabbed.css',
    'resources/assets/pagina/vendor/css/tabbed/tabbed2.css',
    'resources/assets/pagina/vendor/css/tabbed/tabbed3.css',
    'resources/assets/pagina/vendor/css/mdb/compiled.min.css',
    'resources/assets/pagina/vendor/css/font-awesome/font-awesome.min.css',
    'resources/assets/pagina/vendor/css/animate/animate.min.css',
    'resources/assets/pagina/vendor/css/owl-carousel-n/owl.carousel.min.css',
    'resources/assets/pagina/vendor/css/owl-carousel-n/owl.theme.default.min.css',
    'resources/assets/pagina/vendor/css/could-tags/cool-tag-cloud.css',
    'resources/assets/pagina/css/style.css'
], 'public/css/pagina.css')
.scripts([
    'resources/assets/pagina/vendor/js/jquery-n/jquery-3.3.1.min.js',
    'resources/assets/pagina/vendor/js/tether/tether.min.js',
    'resources/assets/pagina/vendor/js/wow/wow.min.js',
    'resources/assets/pagina/vendor/js/mdb/mdb.js',
    'resources/assets/pagina/vendor/js/owl-carousel-n/owl.carousel.min.js',
    'resources/assets/pagina/vendor/js/smooth-scroll/smooth-scroll.min.js',
    'resources/assets/pagina/js/sitio.min.js'
], 'public/js/pagina.js')
.js('resources/assets/js/app2.js','public/js/app2.js');
