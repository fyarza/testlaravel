<?php
/**
 *Promacion : federicoyarza295@gmail.com
 */

return [
    'HOME' =>'',
    'BLOGUERO' => 'Prueba de Laravel',
    'BLOGUERO2' => 'Prueba de Laravel',
    'JOB' => '| Test LARAVEL',
    'HOGAR' => 'VZLA',
    'ESTADO' => 'CARABOBO',
    'CALLE' => 'Sin Direccion',
    'PAIS' => 'VZLA',
    'ZIP' => '2020',
    'POSTALOFICINA' => '',

    'FACEBOOK' => 'https://www.facebook.com/',
    'TWITTER' => 'https://twitter.com/',
    'INSTAGRAM' => 'https://www.instagram.com/',
    'GPLUS' => 'https://plus.google.com/',
    'EMAIL' => 'direccionpostgradoinguc@gmail.com/',
    'LINKEDIN' => 'https://www.linkedin.com/company/',
    'YOUTUBE' => 'https://www.youtube.com/',


/*todo mejorar*/
    'KEYWORDS' => 'TEST | TRABAJO, OPORTUNIDAD,COMPROMISO',
    'TEMA' => 'QUIERES SER UN PROGRAMADOR DE ALTO NIVEL',
    'DESCRIPCION' => 'SI',
    'LOGO' => '',
    'LOGOS' => '',
    'MAINSITE' => '',
    'CANONICAL' => '',
    'PUBLICADO' => '2018-10-22',
    'ULTMOD' => 'domingo, 14 de octubre de 2018 (GMT-4)',
    'EXPIRE' => 'domingo, 14 de octubre de 2018 (GMT-4)'

];