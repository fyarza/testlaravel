<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;


class Post extends Model
{
    protected $fillable = [
        'title', 'body', 'url', 'iframe', 'excerpt', 'published_at', 'category_id', 'user_id','tipo'
    ];

    protected $dates = ['published_at'];
    //protected $with = ['category', 'tags', 'owner', 'photos'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($post)
        {
            
            $post->tags()->detach();
            if(!empty($post->photo)){
                foreach ($post->photo as $photo) {
                    $photo->delete();
                }
            }
            
        });
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /*sobreescribo o cambio de metodo de busqueda de Id a titulo del post*/

    public function getRouteKeyName()
    {
        return 'url';
    }


    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function comments(){
        return $this->hasMany(Comment::class,'post_id','id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopePublished($query)
    {
        $request = request();

        $query->with(['category', 'tags', 'owner', 'photos'])
            ->whereNotNull('published_at')
            ->where('published_at', '<=', Carbon::now())
            ->orderBy('published_at' , 'DESC')
            ->get();

        if($request->year) {
            $query->whereYear('published_at', $request->year);
        }

        if($request->month) {
            $query->whereMonth('published_at', $request->month);
        }
    }

    public function scopeBlogPublished($query)
    {
        $request = request();

        $query->with(['category', 'tags', 'owner'])
            ->whereNotNull('published_at')
            ->where('published_at', '<=', Carbon::now())
            ->latest('published_at')
            ->get();

        if($request->year) {
            $query->whereYear('published_at', $request->year);
        }

        if($request->month) {
            $query->whereMonth('published_at', $request->month);
        }
        if($request->title) {
            $query->where('title', 'LIKE', "%$request->title%");
        }
    }

    public function scopeAllowed($query)
    {
        if ( auth()->user()->can('view', $this) )
        {
            $posts = $query;
        }
        else
        {
            $posts = $query->where('user_id', auth()->id());
        }
    }

    public function isPublished()
    {
        return ! is_null($this->published_at) && $this->published_at < Carbon::today();
    }

    public static function create(array $attributes = [])
    {
        $attributes['user_id'] = auth()->id();
        /*--------------------------------------------------
        Modificacion de la forma de como se guarda los post 
        a la forma tradicional
        ----------------------------------------------------- */
        $post = new Post();
        $post->title = $attributes['title'];
        $post->user_id = $attributes['user_id'];
        $post->generateUrl();
        return $post;
    }

    public function generateUrl()
    {
        $url = str_slug($this->title);

        if ($this->whereUrl($url)->exists())
        {
            $url = "{$url}-{$this->id}";
        }
        $this->url = $url;

        $this->save();
    }

    public function scopeByYearAndMonth($query)
    {
        return $query->selectRaw('year(published_at) year')
            ->selectRaw('month(published_at) month')
            ->selectRaw('monthname(published_at) monthname')
            ->selectRaw('count(*) posts')
            ->groupBy('year', 'month', 'monthname', 'published_at')
            ->orderBy('year', 'month');
    }

    /*Metodo Mutador de Fecha de Publicacion*/
    public function setPublishedAtAttribute($published_at)
    {
        $this->attributes['published_at'] = $published_at ? Carbon::parse($published_at) : null;
    }

    /*Metodo Mutador de Categorias*/
    public function setCategoryIdAttribute($category)
    {
        $this->attributes['category_id'] = Category::find($category)
            ? $category
            : Category::create(['name' => $category])->id;
    }

    /*Funcion normal Synctags*/

    public function syncTags($tags)
    {       
        /* se cambio para que los tag fueran int para la sincronizacion */
        $tagIds = collect($tags)->map(function ($tag) {
            return Tag::find($tag) ? $tag : (string) Tag::create(['name' => $tag])->id;
        });
        return $tagIds;
    }

    public function viewType($home = '')
    {
        if($this->iframe):
            return 'posts.iframe';
        endif;
        return 'posts.text';
    

    }
    public function viewPostType($home = '')
    {
        if($this->iframe):
            return 'homePosts.iframe';
        endif;
        if($this->photos->count() === 1):
            return 'homePosts.photo';
        elseif($this->photos->count() > 1):
            return $home === 'home' ? 'homePosts.carousel-preview' : 'homePosts.carousel' ;
        else:
            return 'homePosts.text';
        endif;

    }
}
