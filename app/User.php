<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;

class User extends Model implements Authenticatable
{
    use Notifiable,HasRolesAndPermissions,AuthenticatableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRoleName()
    {
        return $this->roles()->pluck('name')->implode(', ');
    }

    public function getRoleDisplayNames()
    {
        return $this->roles()->pluck('name')->implode(', ');
    }

    public function posts(){
        return $this->hasMany(Post::class,'user_id','id');
    }

    public function comments(){
        return $this->hasMany(Comment::class,'user_id','id');
    }


}
