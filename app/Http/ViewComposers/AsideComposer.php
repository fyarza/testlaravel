<?php

namespace App\Http\ViewComposers;
use App\Tag;
use App\Category;
use Illuminate\Contracts\View\View;


class AsideComposer {
    public function compose (View $view) {
        $categories = Category::withCount('posts')
            ->with('posts')
            ->has('posts', '>=', 1)
            ->orderBy('posts_count', 'DESC')
            ->get();
        $tags = Tag::withCount('posts')
            ->with('posts')
            ->has('posts', '>=', 1)
            ->orderBy('posts_count', 'DESC')
            ->get();
        $view->with('categories', $categories)->with('tags', $tags);
    }
}