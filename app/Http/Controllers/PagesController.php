<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    public function index(){
        $posts = Post::BlogPublished()->paginate(15);
        return view('welcome',compact('posts'));
    }
}
