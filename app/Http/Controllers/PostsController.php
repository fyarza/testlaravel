<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StorePostRequest;
use Illuminate\Http\Request;
use App\Post;
use App\Tag;
use Laracasts\Flash\Flash;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::paginate(15);
        // return $posts;
    	return view('admin.posts.index', compact('posts'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required|min:3']);
        $post = Post::create( $request->except(['_token']) );
        return redirect()->route('posts.edit', $post);
    }

    public function edit(Post $post)
    {      
        return view('admin.posts.edit', [
            'post' => $post,
            'tags' => Tag::all(),
            'categories' => Category::all()
        ]);
    }


    public function show(Post $post)
    {
              
        if($post->isPublished() || auth()->check())
        {
            return view('posts.show', compact('post'));
        }
        abort(404);
    }

    public function update(Post $post, StorePostRequest $request)
    {
         $post->update($request->all());
        $tagss=$request->input('tags');
        $tags=array();
        foreach($tagss as $tag){
            $temp=Tag::find($tag) ? $tag : (string) Tag::create(['name' => $tag])->id;
            array_push($tags,$temp);
        }
        $post->tags()->sync( $tags);
        Flash::success('La publicación ha sido guardada.');
        return redirect()
            ->route('posts.index', $post);
    }

    public function destroy(Post $post)
    {
        $post->delete();
        Flash::success('La publicación ha sido eliminada');
        return redirect()
            ->route('posts.index');
    }
}
