<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('init');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Routes del Sistema
Route::middleware(['auth'])->group(function(){
   
    /*------------------------------------------
     Deficion de Rutas para los Roles del Sistema
     -------------------------------------------- */
    Route::resource('roles', 'RoleController');
    /*------------------------------------------
     Deficion de Rutas para los Usuarios del Sistema
     -------------------------------------------- */
    Route::resource('users', 'UserController');
    /*------------------------------------------
     Deficion de Rutas para los Post del Sistema
     -------------------------------------------- */
    Route::resource('posts', 'PostsController');
});
